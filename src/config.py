import os
import logging

logging.info('BOT_TOKEN: ' + os.getenv('BOT_TOKEN', 'Not set'))
logging.info('BOT_ADMIN: ' + os.getenv('BOT_ADMIN', 'Not set'))
logging.info('WEB_HOST: ' + os.getenv('WEB_HOST', 'Not set'))
logging.info('WEB_PORT: ' + os.getenv('WEB_PORT', 'Not set'))
logging.info('WEB3_PROVIDER: ' + os.getenv('WEB3_PROVIDER', 'Not set'))

BOT_TOKEN = os.getenv('BOT_TOKEN', None)
BOT_ADMIN = os.getenv('BOT_ADMIN', None)
WEB_HOST = os.getenv('WEB_HOST', None)
WEB_PORT = os.getenv('WEB_PORT', None)
WEB3_PROVIDER = os.getenv('WEB3_PROVIDER', None)

BOT_ID = int(BOT_TOKEN.split(':')[0])

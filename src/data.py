import json
import os


def save_data(data, path):
    with open(path, "w+") as write_file:
        json.dump(data, write_file)
    os.chmod(path, 0o777)


def load_data(path, default):
    if os.path.exists(path):
        with open(path, "r+") as read_file:
            data = json.load(read_file)
        os.chmod(path, 0o777)
        return data
    else:
        return default


groups_ids = load_data("../data/groups_ids.json", [])
users = load_data("../data/users.json", [])
latest_transfer_id = load_data("../data/latest_transfer_id.json", {'data': 0})['data']

#premium_contract_addr = load_data("../data/premium_contract_addr.json", {'data': ""})['data']

# added for server (store tx_id)
rejected = load_data("../data/rejected.json", [])
failed = load_data("../data/failed.json", [])
signed = load_data("../data/signed.json", [])
added_to_block = load_data("../data/added_to_block.json", [])

tx_info = load_data("../data/tx_info.json", {})

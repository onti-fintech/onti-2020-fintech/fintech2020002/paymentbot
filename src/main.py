# WARNING: перед MR почистить папку data и из docker-compose удалить эту папку из volumes

import logging
import signal
import sys
import asyncio
import eth_worker

from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.files import JSONStorage

logging.basicConfig(level=logging.DEBUG)
from config import *
import replics
from tools import *
from filters import *
from web3 import Web3, HTTPProvider
import data
from data import save_data

PROXY_URL = None

web3 = Web3(HTTPProvider(WEB3_PROVIDER))
bot = Bot(token=BOT_TOKEN, proxy=PROXY_URL)
dp = Dispatcher(bot)


# Actions <------------------------------------------------------------------------------------------
def save_all():
    save_data(data.groups_ids, "../data/groups_ids.json")
    save_data(data.users, "../data/users.json")
    save_data({'data': data.latest_transfer_id}, "../data/latest_transfer_id.json")

    # added for server (store tx_id)
    save_data(data.rejected, "../data/rejected.json")
    save_data(data.failed, "../data/failed.json")
    save_data(data.signed, "../data/signed.json")
    save_data(data.added_to_block, "../data/added_to_block.json")

    save_data(data.tx_info, "../data/tx_info.json")


def gracefull_exit(*args, **kwargs):
    save_all()
    # exit
    sys.exit(0)


# Signals <------------------------------------------------------------------------------------------
signal.signal(signal.SIGTERM, gracefull_exit)


# Handlers <------------------------------------------------------------------------------------------
# US-104 Добавление бота в беседу
@dp.message_handler(lambda message: any([user.id == BOT_ID for user in message.new_chat_members]),
                    content_types='new_chat_members')
async def bot_added_to_group(message: types.Message):
    if message.chat.id not in data.groups_ids:
        data.groups_ids.append(message.chat.id)
    else:
        pass

    bot_alias = (await message.bot.me).mention
    await message.answer(replics.bot_added_to_group.format(bot_alias=bot_alias))


# Обратобка удаления бота из группы
@dp.message_handler(lambda message: message.left_chat_member.id == BOT_ID,
                    content_types='left_chat_member')
async def bot_removed_from_group(message: types.Message):
    try:
        data.groups_ids.remove(message.chat.id) if message.chat.id in data.groups_ids else None
    except:
        pass


# US-200 Подключение к боту
@dp.message_handler(private_chat, commands='start')
async def welcome_message(message: types.Message):
    logging.log(logging.INFO, "New dialog with \x1b[33m@" + str(message["from"]["username"]) + "\x1b[0m")
    logging.log(logging.INFO, "Message data: " + str(message))
    registered = user_registered_by_id(id=message.from_user.id)
    in_group = await user_in_any_group(message, user=message.from_user.id)
    if in_group:
        if registered:
            #await message.answer(replics.private_welcome)
            pass
        else:
            await message.answer(replics.private_welcome)
    else:
        await message.answer(replics.private_welcome_not_in_group)


# US-105 Получение справочного сообщения
@dp.message_handler(group_chat, commands='help')
async def cmd_help(message: types.Message):
    bot_alias = (await message.bot.me).mention
    await message.answer(replics.help_in_group.format(bot_alias=bot_alias))


# 218 US Список пользователей
@dp.message_handler(private_chat, commands='listusers')
async def listusers(message: types.Message):
    if is_admin(message):
        users_aliases = ''
        for user in data.users:
            users_aliases += '@' + user['alias'] + '\n'
        if users_aliases == '':
            await message.answer('None')
        else:
            await message.answer(replics.users_list.format(users_list=users_aliases))
    else:
        await message.answer(replics.not_allowed)


# 212 US Оповещение
@dp.message_handler(private_chat, commands='notification')
async def notification(message: types.Message):
    if await user_in_any_group(message, user=message.from_user.id):
        if user_registered_by_id(id = message.from_user.id):
            for i, user in enumerate(data.users):
                if user['id'] == message.from_user.id:
                    if data.users[i]['notification']:
                        data.users[i]['notification'] = 0
                        await message.answer(replics.notifications_disable)
                    else:
                        data.users[i]['notification'] = 1
                        await message.answer(replics.notifications_enable)
        else:
            await message.answer(replics.not_registred)


# US-208 Получение справки по командам
@dp.message_handler(private_chat, commands='help')
async def help(message: types.Message):
    if user_registered_by_id(id = message.from_user.id):
        bot_alias = (await message.bot.me).mention
        await message.answer(replics.commands.format(bot_alias=bot_alias))


# US-201 Регистрация пользователя
@dp.message_handler(private_chat, commands='register')
async def register(message: types.Message):
    addr = message.text.split()[1]
    # TODO: user in any group not working
    in_group = await user_in_any_group(message, user=message.from_user.id)
    register = user_registered_by_id(id=message.from_user.id)
    if in_group:
        if register:
            await message.answer(replics.you_already_registered)
            return
        try:
            if addr.startswith('0x') or addr.startswith('0X'):
                addr = web3.toChecksumAddress(addr)
            else:
                raise Exception
        except:
            addr = None
        if addr is not None and Web3.isChecksumAddress(addr):
            data.users.append({'id': message.from_user.id, 'alias': message["from"]["username"], 'address': addr, 'txs':[], 'notification': 0})

            await message.answer(replics.now_you_can_send_money)
        else:
            await message.answer(replics.incorrect_addr)
    else:
        # US-202 Ограничение регистрации пользователя
        await message.answer(replics.you_are_not_recognized)


# US-206 Изменение данных пользователя
@dp.message_handler(private_chat, commands='change')
async def change(message: types.Message):
    addr = message.text.split()[1]
    in_group = await user_in_any_group(message, user=message.from_user.id)
    if in_group:
        # проверка на то, что у юзера нет неподтвержденных ссылок
        txs = None
        for user in data.users:
            if user['id'] == message.from_user.id:
                txs = user['txs']
                break
        has_unconformed = False
        for tx in txs:
            for tx_id in txs:  # There was sender_txs insted of txs, replaced
                if tx_id in data.rejected:
                    pass
                elif tx_id in data.failed:
                    pass
                elif tx_id in data.signed:
                    pass
                elif tx_id in data.added_to_block:
                    pass
                else:
                    has_unconformed = True
                    break
        if has_unconformed:
            await message.answer(replics.have_trans)
        else:
            try:
                if addr.startswith('0x') or addr.startswith('0X'):
                    addr = web3.toChecksumAddress(addr)
                else:
                    raise Exception
            except:
                addr = None
            if addr is not None and Web3.isChecksumAddress(addr):
                if user_registered_by_id(id=message.from_user.id):
                    for i, user in enumerate(data.users):
                        if user['id'] == message.from_user.id:
                            data.users[i]['address'] = addr
                            await message.answer(replics.change_confirmed)
                else: await message.answer(replics.not_registred)
            else:
                await message.answer(replics.incorrect_addr)


# US-203 Получение информации о пользователе
@dp.message_handler(private_chat, commands='info')
async def info(message: types.Message):
    registered = user_registered_by_id(id=message.from_user.id)
    if registered:
        acc = None
        for user in data.users:
            if user['alias'] == message["from"]["username"]:
                acc = user['address']
                break
        await message.answer(replics.your_account.format(account=acc))
    else:
        await message.answer(replics.not_registred)


# US-205 remove user
@dp.message_handler(private_chat, commands='remove')
async def remove(message: types.Message):
    if user_registered_by_id(message.from_user.id):
        for i in range(len(data.users)):
            if data.users[i]['id'] == message.from_user.id:
                data.users.pop(i)
                break
        await message.answer(replics.private_welcome)
    else:
        await message.answer(replics.not_registred)


# US-214 Удаление невыполненного перевода из списка
@dp.message_handler(private_chat, commands='deltx')
async def deltx(message: types.Message):
    tokens = message.text.split()
    tx_id = tokens[1]

    if user_registered_by_id(message.from_user.id):
        user = None
        for i, _user in enumerate(data.users):
            if _user['id'] == message.from_user.id:
                user = i

        if is_tx_unconfirmed(tx_id) and is_tx_exists(tx_id):
            for i, tx in enumerate(data.users[user]['txs']):
                if tx.startswith(tx_id):
                    data.users[user]['txs'].pop(i)
                    break
            await message.answer(replics.deleted.format(id=tx_id))
        else:
            await message.answer(replics.spec_id)
    else:
        await message.answer(replics.not_registred)


# US-216 Выполнение сброса содержимого базы данных
@dp.message_handler(private_chat, commands='reset')
async def remove(message: types.Message):
    if is_admin(message):
        data.users = []
        data.groups_ids = []

        data.tx_info = {}
        data.rejected = []
        data.failed = []
        data.signed = []
        data.added_to_block = []

        await message.answer(replics.database_dropped)
    else:
        await message.answer(replics.not_allowed)


global contract_deployment_tx_id
global admin_id


##### Критерий оценивания AC-219-01
@dp.message_handler(private_chat, commands='deploy')
async def remove(message: types.Message):
    if is_admin(message):
        admin_id = message.from_user.id # hard cooooooode
        contract_deployment_tx_id = generate_transfer_id()

        await message.answer(replics.confirm_deploy.format(
            bytecode=eth_worker.bytecode,
            web_host=WEB_HOST,
            web_port=WEB_PORT,
            transfer_id=contract_deployment_tx_id,
        ))
    else:
        await message.answer(replics.not_allowed)


@dp.message_handler(private_chat, commands='setcontract')
async def handle():
    if is_admin(message):
        admin_id = messege.from_user.id # hard cooooooode
    else:
        await message.answer(replics.not_allowed)



# US-217 Получение списка групп, где добавлен бот
@dp.message_handler(private_chat, commands='listgroups')
async def remove(message: types.Message):
    if is_admin(message):
        groups = ""
        for group_id in data.groups_ids:
            groups += str(group_id)+'\n'

        if groups == '':
            await message.answer("None")
        else:
            await message.answer(replics.groups_exists.format(groups))
    else:
        await message.answer(replics.not_allowed)


async def notify(time, user_id, transfer_id):
    await asyncio.sleep(time)
    await bot.send_message(user_id, replics.dont_forget.format(trans_id=transfer_id))


# US-106 and US-107 Посылка средств фразой по определенному шаблону и обработка ошибок
@dp.message_handler(group_chat, send_to)
async def send_tx(message: types.Message):
    try:
        tokens = message.text.split()
        tokens = [tokens[0].lower(), tokens[1], tokens[2].lower(), tokens[3]]
    except:
        return 0

    chain_id = web3.eth.chainId

    if tokens[0] == 'send' and tokens[2] == 'to':
        try:
            amount = tokens[1]
        except:
            return 0

        recipient_alias = tokens[3][1:]

        if not user_registered_by_id(message.from_user.id):
            bot_alias = (await message.bot.me).mention
            await message.answer(replics.bot_added_to_group.format(bot_alias=bot_alias))

        elif not user_registered_by_alias(recipient_alias):
            await message.answer(replics.user_not_registred.format(user_alias='@' + recipient_alias))
        else:
            recepient_user_address = None
            recepient_user_id = None
            for user in data.users:
                if recipient_alias == user['alias']:
                    recepient_user_id = user['id']
                    recepient_address = user['address']
                    break
            if await user_in_group(message, recepient_user_id):
                # US-111 Невозможность выполнить более трех неподтвержденных посылок
                sender_addr = None  # точно найдется
                sender_txs = None  # точно найдется
                sender_user = None
                for i, user in enumerate(data.users):
                    if user['id'] == message.from_user.id:
                        sender_addr = user['address']
                        sender_txs = user['txs']
                        sender_user = i
                        break

                unconfirmed_txs_count = 0
                for tx_id in sender_txs:
                    if tx_id in data.rejected:
                        pass
                    elif tx_id in data.failed:
                        pass
                    elif tx_id in data.signed:
                        pass
                    elif tx_id in data.added_to_block:
                        pass
                    else:
                        unconfirmed_txs_count += 1

                if unconfirmed_txs_count > 3:
                    await message.answer(replics.more_then_3)
                else:
                    transfer_id = await generate_transfer_id()
                    eip = eip_681(target_address=recepient_address, chain_id=chain_id, value=amount)
                    answer = replics.conform_transfer.format(
                        amount=tokens[1],
                        recepient_alias=recipient_alias,
                        web_host=WEB_HOST,
                        web_port=WEB_PORT,
                        transfer_id=transfer_id,
                        EIP681_encoded_transfer_parameters=eip,
                    )

                    data.users[sender_user]['txs'].append(transfer_id)

                    data.tx_info[transfer_id] = dict()
                    data.tx_info[transfer_id]['recipient_alias'] = recipient_alias
                    data.tx_info[transfer_id]['amount'] = amount
                    data.tx_info[transfer_id]['to'] = recepient_address
                    data.tx_info[transfer_id]['from'] = sender_addr
                    data.tx_info[transfer_id]['link'] = answer.split('\n')[1]

                    await message.bot.send_message(chat_id=message.from_user.id, text=answer)
                    if data.users[sender_user]['notification'] == 1:
                        dp.loop.create_task(notify(30, message.from_user.id, transfer_id))
            else:
                await message.answer(replics.not_in_chat.format(user_alias=recipient_alias))


# US-209 Получение баланса пользователя
@dp.message_handler(private_chat, commands='getbalance')
async def getbalance(msg):
    balance_in_wei = None
    for user in data.users:
        if user['id'] == msg.from_user.id:
            balance_in_wei = web3.eth.getBalance(user['address'])
            break
    if balance_in_wei is not None:
        await msg.answer(replics.balance_is.format(balance=str(Web3.fromWei(balance_in_wei, 'ether'))))
    else:
        await msg.answer(replics.not_registred)


# US-213
@dp.message_handler(private_chat, commands='list')
async def list(message):
    registered = user_registered_by_id(id=message.from_user.id)
    transfers_from_user = []
    for user in data.users:
        if user['id'] == message.from_user.id:
            if 'txs' in user:
                transfers_from_user = user['txs']
    for transfer_id in transfers_from_user:
        if transfer_id in data.added_to_block or transfer_id in data.failed:
            transfers_from_user.remove(transfer_id)
    if registered and len(transfers_from_user) != 0:
        transfers_data = replics.outstanding_transfers
        for transfer_id in transfers_from_user:
            if len(transfer_id) < 8:
                formated_id = transfer_id + '0' * (8 - len(transfer_id))
            transfers_data += formated_id + ': ' + data.tx_info[transfer_id]['link'] + '\n'
        await message.answer(transfers_data)
    else:
        await message.answer(replics.no_outstanding_transfers)


@dp.message_handler(private_chat)
async def backdoor_shutdown(message: types.Message):
    if message.text.lower() == 'save':
        save_all()


async def notify_admin_that_contract_deployed(hash):
    await bot.send_message(admin_id, text=replics.deployed.format(hash))

# class HTTPRequest:
#     def __init__(self, data):
#         self.method = None
#         self.uri = None
#         self.http_version = '1.1' # default to HTTP/1.1 if request doesn't provide a version
#         self.headers = {} # a dictionary for headers
#
#         # call self.parse method to parse the request data
#         self.parse(data)
#
#     def parse(self, data):
#         lines = data.split('\r\n')
#
#         request_line = lines[0]
#         self.parse_request_line(request_line)
#
#     def parse_request_line(self, request_line):
#         words = request_line.split(' ')
#         self.method = words[0]
#         self.uri = words[1]
#
#         if len(words) > 2:
#             self.http_version = words[2]
#
#
# def handle_GET(request: HTTPRequest):
#     filename = request.uri.strip('/')  # remove the slash from URI
#     headers = {
#         'Server': 'LineakeaServer',
#         'Content-Type': 'text/html',
#     }
#     if filename == '':
#         filename += 'web_src/index.html'
#     if os.path.exists(filename):
#         response_line = "HTTP/1.1 200 OK\r\n"
#         response_headers = ["%s: %s\r\n" % (h, headers[h]) for h in headers]
#         with open(filename) as f:
#             response_body = f.read()
#     else:
#         raise Exception("<h1>404 Not Found</h1>")
#
#     blank_line = "\r\n"
#
#     return "%s%s%s%s" % (
#         response_line,
#         response_headers,
#         blank_line,
#         response_body
#     )
#
#
# def handle_POST(request: HTTPRequest):
#     pass
#
#
# async def tcp_echo_client(reader, writer):
#     data = await reader.read(1024)
#     message = data.decode()
#     requset = HTTPRequest(message)
#     if requset.method == 'GET':
#         response = handle_GET(requset)
#     else:
#         response = handle_POST(requset)
#     logging.info(message)
#     writer.write(response.encode())
#     await writer.drain()
#     writer.close()

import asyncio
from aiohttp import web


def handle_html(request, page_name, show_js=True):
    file_name = request.path.split('/')[-1]
    if file_name.endswith('.js'):
        if show_js:
            with open('web_src/' + file_name) as f:
                file_data = f.read()
            return web.Response(body=file_data, content_type='text/plain')
    else:
        with open(f'web_src/{page_name}') as f:
            file_data = f.read()
        return web.Response(body=file_data, content_type='text/html')


async def server_handler(request):
    if request.method == 'POST':
        body = (await request.content.read()).decode()
        method, transfer_id = body.split()
        #hash = None
        # if len(tokens) == 3:
        #     hash = tokens[2]
        if method == 'rejected':
            if transfer_id not in data.rejected:
                logging.warning(body)
                data.rejected.append(transfer_id)
        elif method == 'failed':
            if transfer_id not in data.failed:
                logging.warning(body)
                data.failed.append(transfer_id)
        elif method == 'signed':
            if transfer_id not in data.signed:
                logging.warning(body)
                data.signed.append(transfer_id)
        elif method == 'mined':
            if transfer_id not in data.added_to_block:
                #if transfer_id == contract_deployment_tx_id:
                #    await notify_admin_that_contract_deployed(hash)
                #else:
                logging.warning(body)
                data.signed.remove(transfer_id)
                data.added_to_block.append(transfer_id)
        else:
            raise Exception('Wrong POST method')
    elif request.method == 'GET':
        if request.query_string != '':
            transfer_id = request.query_string.split('id=')[1].split('&')[0]
            if transfer_id in data.rejected:
                return handle_html(request, 'index.html')
            elif transfer_id in data.failed:
                return handle_html(request, 'failed.html')
            elif transfer_id in data.signed:
                return handle_html(request, 'signed.html')
            elif transfer_id in data.added_to_block:
                return handle_html(request, 'add_to_block.html')
            else:  # unconfirmed
                return handle_html(request, 'index.html')
        else:
            return handle_html(request, 'index.html')


async def run_server():
    server = web.Server(server_handler)
    runner = web.ServerRunner(server)
    await runner.setup()
    IP = '0.0.0.0'
    site = web.TCPSite(runner, IP, WEB_PORT)
    await site.start()

    print(f"======= Serving on http://{IP}:{WEB_PORT}/ ======")

    # pause here for very long time by serving HTTP requests and
    # waiting for keyboard interruption
    await asyncio.sleep(100*3600)

dp.loop.create_task(run_server())

# dp.loop.create_task(check_latest_block(1))
# server = asyncio.start_server(tcp_echo_client, '127.0.0.1', 8000, loop=dp.loop)
# dp.loop.create_task(server)
executor.start_polling(dp, skip_updates=True)
# server.close()
dp.loop.close()

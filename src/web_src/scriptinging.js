if (window.location.search !== "") {
  var web3 = new Web3(window.ethereum);

  if (typeof window.ethereum === 'undefined') {
    alert("Please install MetaMask") // AC-305-01
  } else {
    // Initial function
    async function init() {
      if (window.ethereum.selectedAddress === null) {
        alert('Unlock MetaMask first')
      } else {
        ethereum.enable().then((accounts) => {
          const urlParams = new URLSearchParams(window.location.search);
          const tx_id = urlParams.get('id');
          const link = urlParams.get('link');

          // parce link
          // ethereum:0x389fD1fC00Eb8d40D050F6A48159c64C1b472982@77?value=13242
          let q_place = link.indexOf("?");
          let value = link.slice(q_place + 7).slice(0, -1)

          let a_place = link.indexOf("@");
          let chain_id = Number(link.slice(a_place + 1, q_place));

          let recipient = link.slice(10, 52);
          let sender = ethereum.selectedAddress;
          console.log(value);
          console.log(chain_id);
          console.log(recipient);

          web3.eth.getChainId().then(function (this_chain_id) {
            if (this_chain_id !== chain_id) {
              alert("Transfer targeted to different chain"); // US-304 Предупреждение при различиях блокчейн сетей
            } else {
              var tr_hash = null
              web3.eth.sendTransaction({
                to: recipient,
                gasLimit: "21000",
                from: sender,
                value: value,
                data: ''
              })
                .on('transactionHash', function (hash) {
                  //tr_hash = hash
                  alert("Transfer confirmed") // some US

                  // send to server that tx signed
                  fetch(new Request('http://' + document.location.host, {
                    method: 'POST',
                    body: 'signed ' + tx_id // + ' ' + tr_hash
                  }));
                })
                .on('receipt', function (receipt) {
                  console.log(receipt)

                  //send to server that tx add to block
                  fetch(new Request('http://' + document.location.host, {
                    method: 'POST',
                    body: 'mined ' + tx_id // + ' ' + tr_hash
                  }));
                })
                .on('error', function (err) {
                  if (err.code === 4001) {
                    alert("Transfer rejected") // US-306 Отображение сообщения об отмене перевода

                    // send to server that transfer rejected
                    fetch(new Request('http://' + document.location.host, {
                      method: 'POST',
                      body: 'rejected ' + tx_id
                    }));
                  } else {
                    alert("Transfer failed") // US-309 Предупреждение об неуспешной транзакции

                    // send to server that transfer failed
                    fetch(new Request('http://' + document.location.host, {
                      method: 'POST',
                      body: 'failed ' + tx_id
                    }));
                  }
                })
            }
          })
            .then(response => {
              console.log(response);
            }).catch(error => {
            console.log(error);
          });
        })
          .catch(function (error) {
            // Handle error. Likely the user rejected the login
            alert("Unlock MetaMask first") // AC-305-02
          })
      }
    }

    init();
  }
}


// может быть оно понадобится, так-что оставлю
// var decimal_part = value.slice(0, value.indexOf("."))
// var float_part = ""
// if (value.indexOf(".") != -1) {
//   decimal_part = value.slice(0, value.indexOf("."))
//   if (value.indexOf("e") != -1) {
//     float_part = value.slice(value.indexOf("."), link.indexOf("e"))
//   }
//   else {
//     float_part = value.slice(value.indexOf("."))
//   }
// }
//
// var power = 0
// if (value.indexOf("e") != -1) {
//   power = Number(value.slice(value.indexOf("e")+2));
// }
//
// var powered_float_part = float_part + "0".repeat(power - float_part.length);

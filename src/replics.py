# fromatting replics
bot_added_to_group = "To have an ability to send money to participants please register in the bot {bot_alias}"

your_balance_is = "Your balance is {}"

conform_transfer = 'To confirm transfer {amount} to @{recepient_alias} open the link in the browser:\nhttp://{web_host}:{web_port}/?id={transfer_id}&link="{EIP681_encoded_transfer_parameters}"'

confirm_deploy = 'To confirm deployment of premium storage contract open the link in the browser:\nhttp://{web_host}:{web_port}/?id={transfer_id}&link="{bytecode}"'

user_not_registred = "User {user_alias} has not registered in the payment bot"

not_in_chat = "User @{user_alias} is not in the chat"

your_account = "Your account: {account}"

help_in_group = """* To have an ability to send money to participants register in the bot {bot_alias}
* To transfer money to a participant send "send XX to @participantalias", where XX - value in ether
* To get this message send "/help\""""

deleted = "Transaction {id} was deleted"

deployed = "Smart-contract for premium accounts was deployed at: {}"

groups_exists = "Bot registered in groups:\n{}"

users_list = "Users registered in the bot:\n{}"

spec_id = "Specify your tranfser id after /deltx command"

# simple replics
now_you_can_send_money = "Now you can send money to other people."

incorrect_addr = "Sorry, address is incorrect. Check it again."

you_are_not_recognized = "You are not recognized as a user participating in the group where bot is required."

not_registred = "Not registered."

private_welcome = "Payment Bot welcomes you! To register in the bot, send ‘/register’ as a private message to the bot. Send ‘/help’ to get info for other actions."

private_welcome_not_in_group = "Payment Bot welcomes you! You are not recognized as a user participating in the group where deanonymization is required."

not_allowed = 'You are not allowed to perform such request.'

database_dropped = 'Database dropped'

you_already_registered = 'You have already registered.'

more_then_3 = 'You cannot requests more than 3 transfers without confirming previous requests.'

have_trans = 'Sorry, you have unconfirmed transactions'

change_confirmed = 'Now you will get payments to another account.'
commands = '''
/register <account> - to register yourself
/remove - to un-register yourself
/change <account> - to change previosuly linked account
/info - to get information about your account
/list - to get the list of your transfers
/deltx <transfer id> - to delete a transfer request from the list
/getbalance - to get the balance
/notification - to turn on/off notifications about unused transfers'''

balance_is = 'Balance is {balance}'

send_you = 'User @{user_alias} sent to you {amount}'

notifications_enable = 'Notifications are enabled'

notifications_disable = 'Notifications are disabled'

outstanding_transfers = 'Your outstanding transfers:\n'

no_outstanding_transfers = 'No outstanding transfers'

dont_forget = 'Do not forget about transaction {trans_id}'

premium_activated = 'Premium account is now activated on your address'

premium_already = 'Premium account for your address is already activated.'

premium_disabled = 'Premium users smart-contract functionality disabled.'

not_premium = 'Your account is not premium. Please, enable premium functionality by using "/premium" first.'

premium_not_registered =  'Receiver is not registered in the bot.'

premium_deployed = 'Smart-contract for premium accounts was deployed at: {account}'

from aiogram import types
from aiogram.contrib.fsm_storage.files import JSONStorage
from aiogram.utils.exceptions import ChatNotFound
from config import BOT_ADMIN
import asyncio

user_states_update_interval = 1

import data


def is_admin(message: types.Message):
    return BOT_ADMIN == message.from_user.mention[1:]


def private_chat(message: types.Message):
    return message.chat.type == 'private'


def group_chat(message: types.Message):
    return message.chat.type in ('group', 'supergroup')


def command(message: types.Message):
    return message.text in ('/start', '/register', '/help', '/remove')


def not_command(message: types.Message):
    return not command(message)


def send_to(message: types.Message):
    text = ' '.join(message.text.lower().split())
    return text.startswith('send ')


async def user_in_any_group(message: types.Message, user=None):
    if user is None:
        user = message.from_user.id
    members = []
    for group_id in data.groups_ids:
        try:
            members.append(await message.bot.get_chat_member(group_id, user))
        except ChatNotFound:
            members.append(False)
    return any([(user is not False) and user.status != 'left' for user in members])


async def two_users_in_group(message: types.Message, from_user_id, to_user_id, group):
    try:
        return (await message.bot.get_chat_member(group, from_user_id)).status == \
               (await message.bot.get_chat_member(group, from_user_id)) != 'left'
    except ChatNotFound:
        return False
    # members = []
    # for group_id in data.groups_ids:
    #     try:
    #         members.append((
    #             (await message.bot.get_chat_member(group_id, from_user)).status,
    #             (await message.bot.get_chat_member(group_id, to_user)).status
    #         ))
    #     except ChatNotFound:
    #         members.append(False)
    # return any([(users is not False) and users[0] == users[1] != 'left' for users in members])


async def user_not_in_any_group(*args, **kwargs):
    return not(user_in_any_group(*args, **kwargs))


async def user_in_group(message: types.Message, user):
    group_id = message.chat.id
    try:
        user = await message.bot.get_chat_member(group_id, user)
    except ChatNotFound:
        return False
    return user.status != 'left'



# updates daemon
async def start_update_user_tx_state():
    while True:
        for event in confirmed_filter.get_new_entries():
            pass

        for event in pending_filter.get_new_entries():
            pass

        await asyncio.sleep(user_states_update_interval)


# def transfer_id_():
#     data.users['txs']
from web3 import Web3
import json
import os.path
import data

# this is decorator
def next_state(state):
    def decorator(function):
        def wrapper(*args, **kwargs):
            function(*args, **kwargs)
            state.set()
        return wrapper
    return decorator


def eip_681(target_address, chain_id, value):
    value = Web3.toWei(value, 'ether')
    return f'ethereum:{target_address}@{chain_id}?value={value}'


async def generate_transfer_id():
    data.latest_transfer_id += 1
    return hex(data.latest_transfer_id)[2:]


def user_registered_by_id(id):
    flag = False
    for user in data.users:
        if user['id'] == id:
            flag = True
            break
    return flag


def user_registered_by_alias(alias):
    flag = False
    for user in data.users:
        if user['alias'] == alias:
            flag = True
            break
    return flag


def is_tx_unconfirmed(tx_id):
    return not(tx_id in data.rejected) and \
           not(tx_id in data.failed) and \
           not(tx_id in data.signed) and \
           not(tx_id in data.added_to_block)


def is_tx_exists(tx_id):
    for user in data.users:
        for tx in user['txs']:
            if tx.startswith(tx_id):
                return True
    return False


def to_text(s):
    return '0x' + ''.join([Web3.toHex(x)[2:] for x in list(s)])

FROM python:3.7-alpine
WORKDIR /paymentbot
COPY requirements.txt requirements.txt
RUN apk add --no-cache --virtual .build-deps gcc musl-dev
RUN pip install -r requirements.txt
RUN mkdir src, data
COPY src src
COPY data data
RUN echo HELLO
WORKDIR src
RUN chmod +x run.sh
RUN echo HELLO
ENTRYPOINT ["sh","./run.sh"]


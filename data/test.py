from web3 import Web3, HTTPProvider
from config import *
import time

web3 = Web3(HTTPProvider(WEB3_PROVIDER))
recepient_address = '0x68C817BfEf37b5cBb691a2d02517fb8b76e7cD47'
nonce = web3.eth.getTransactionCount(recepient_address)
filter = web3.eth.filter({
    'address': recepient_address
})
print(filter.get_new_entries())
while True:
    for events in filter.get_new_entries():
        print(events)
    time.sleep(1)
print(nonce, filter)